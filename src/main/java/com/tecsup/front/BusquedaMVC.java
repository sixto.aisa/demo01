package com.tecsup.front;

import com.tecsup.back.domain.Auto;
import com.tecsup.back.service.AutoService;
import com.tecsup.back.service.AutoServiceImpl;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Image;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Textbox;

/**
 *
 * @author saisa
 */
public class BusquedaMVC extends SelectorComposer {
    
    @Wire
    private Textbox txtBuscar;
    @Wire
    private Listbox lstAutos;
    @Wire
    private Image imgAuto;
    @Wire
    private Label lblModelo;
    @Wire
    private Label lblMarca;
    @Wire
    private Label lblPrecio;
    @Wire
    private Label lblDescripcion;
    
    private ListModel modeloAuto;
    
    private AutoService autoService=new AutoServiceImpl();
    
    @Listen("onClick = #btnBuscar")
    public void onBuscar(){
        
        modeloAuto=new ListModelList(autoService.buscar(txtBuscar.getValue()));
        lstAutos.setModel(modeloAuto);
        
    }
    
    @Listen("onSelect = #lstAutos")
    public void onDetalle(){
        Auto auto=(Auto) modeloAuto.getElementAt(lstAutos.getSelectedIndex());
        
        lblModelo.setValue(auto.getModelo());        
        lblMarca.setValue(auto.getMarca());
        lblPrecio.setValue(auto.getPrecio().toString());
        lblDescripcion.setValue(auto.getDescripcion());
        
        imgAuto.setSrc(auto.getImagen());
        
        
    }
    
    
}
