
package com.tecsup.front;

import com.tecsup.back.domain.Auto;
import com.tecsup.back.service.AutoService;
import com.tecsup.back.service.AutoServiceImpl;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zul.ListModelList;

/**
 *
 * @author saisa
 */
public class BusquedaMVVC {
    
    private String criterio;
    private ListModelList modeloAuto=new ListModelList();
    private Auto seleccionar;
    
    AutoService autoService=new AutoServiceImpl();

    public String getCriterio() {
        return criterio;
    }

    public void setCriterio(String criterio) {
        this.criterio = criterio;
    }

    public ListModelList getModeloAuto() {
        return modeloAuto;
    }

    public void setModeloAuto(ListModelList modeloAuto) {
        this.modeloAuto = modeloAuto;
    }

    public Auto getSeleccionar() {
        return seleccionar;
    }

    public void setSeleccionar(Auto seleccionar) {
        this.seleccionar = seleccionar;
    }
    
    
    
    
    @Command
    public void buscar(){
        modeloAuto.clear();
        modeloAuto.addAll(autoService.buscar(this.criterio));
    }
    
    
    
}
