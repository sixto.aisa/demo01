package com.tecsup.front;

import com.google.protobuf.Message;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;

/**
 *
 * @author saisa
 */
public class Hola extends SelectorComposer {
    
    @Wire
    private Textbox txtNombre;
    
    
    @Listen("onClick = #btnProcesar ")
    public void onProcesar(){
        Messagebox.show(txtNombre.getValue(),"Información", Messagebox.OK, Messagebox.INFORMATION);
    }
    
}
