package com.tecsup.back.dao;

import com.tecsup.back.domain.Auto;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author saisa
 */
public class AutoDAO {

    
    public static List<Auto> data(){
        List<Auto> listaRetorno=new ArrayList<>();
        int id=1;
        listaRetorno.add(new Auto(id++, "Primera", "Nissan", "La gasolina de 1.6 litros de nivel de entrada se siente poco potente para un automóvil grande. La gasolina de 1.8 litros es entusiasta, la refinada unidad de 2.0 litros es la estrella. Un turbodiésel de 2.2 litros mejorado funciona bien, pero solo es relativamente económico, ya que sus competidores en esta clase con características similares ofrecen un consumo de combustible aún menor.", "/img/car1.png", 23320));
        listaRetorno.add(new Auto(id++, "Cefiro", "Nissan","El Nissan Cefiro es una gama de automóviles de tamaño intermedio que se vende en Japón y otros países. Solo estaba disponible como sedán de 4 puertas. Una gran proporción estaba equipada con transmisiones automáticas. Originalmente comercializado para el asalariado japonés, el modelo superior utilizaba el mismo motor que el del Nissan Skyline R32, un motor de 6 cilindros turboalimentado de 2 litros capaz de producir algo más de 200 CV (150 kW). Otras variantes vinieron con otras versiones del motor Nissan RB. Nuevo, el Cefiro era un poco más caro que el Nissan Skyline equivalente.", "/img/car2.png", 38165));
        
         listaRetorno.add(
                new Auto(id++,
                        "Camry",
                        "Toyota",
                        "The Toyota Camry is a midsize car manufactured by Toyota in Georgetown, Kentucky, USA; as well as Australia; and Japan. Since 2001 it has been the top selling car in the United States.The Holden equivalents were not successful even though they came from the same factory as the Camry. Since 2000 Daihatsu has sold a Camry twin named the Daihatsu Altis. The name comes from the English phonetic of the Japanese word \"kan-muri,\" which means \"crown.\"",
                        "/img/car3.png",
                        24170));
        listaRetorno.add(
                new Auto(id++,
                        "Century",
                        "Toyota",
                        "El Toyota Century es una gran limusina de cuatro puertas producida por Toyota principalmente para el mercado japonés. La producción del Century comenzó en 1967 y el modelo recibió solo cambios menores hasta su rediseño en 1997. Este Century de segunda generación todavía se vende en Japón.",
                        "/img/car4.png",
                        28730));
        listaRetorno.add(
                new Auto(id++,
                        "Sigma",
                        "Mitsubishi",
                        "La tercera generación del automóvil japonés Mitsubishi Galant, que data de 1976, se dividió en dos modelos: el Galant Sigma (para el sedán y la camioneta) y el Galant Lambda (el cupé). El primero se vendió en muchos mercados como Mitsubishi Galant (sin la palabra 'Sigma') y en Australia como Chrysler Sigma (hasta 1980, después de lo cual se convirtió en Mitsubishi Sigma). Curiosamente, en Nueva Zelanda se le identificó como 'Galant Sigma', pero coloquialmente se le conoce como 'Sigma', un nombre que adoptó formalmente después de 1980.",
                        "/img/car5.png",
                        54120));
        listaRetorno.add(
                new Auto(id++,
                        "Challenger",
                        "Mitsubishi",
                        "El Mitsubishi Challenger, llamado Mitsubishi Pajero Sport en la mayoría de los mercados de exportación, Mitsubishi Montero Sport en los países de habla hispana (incluida América del Norte), Mitsubishi Shogun Sport en el Reino Unido y Mitsubishi Nativa en América Central y del Sur (el nombre Challenger también se utilizó en Australia). ), es un SUV de tamaño mediano construido por Mitsubishi Motors Corporation. Fue lanzado en 1997 y todavía está construido a partir de 2006, aunque ya no está disponible en su Japón natal desde finales de 2003.",
                        "/img/car6.png",
                        58750));
        listaRetorno.add(
                new Auto(id++,
                        "Civic",
                        "Honda",
                        "La octava generación de Honda Civic se produce desde 2006. Está disponible como cupé, hatchback y sedán. Los modelos producidos para el mercado norteamericano tienen un estilo diferente. Actualmente hay cuatro motores de gasolina y uno diesel desarrollados para este automóvil. La gasolina de 1.4 litros es más adecuada para la conducción tranquila por la ciudad. El 1.8 litros de gasolina, que desarrolla 140 caballos de fuerza, es un actor dispuesto. El diesel de 2.2 litros es similar a la unidad del Accord. Acelera rápidamente y también es económico. El Honda Civic también está disponible con motor híbrido. En este caso, el motor está acoplado únicamente a una transmisión automática. El modelo de 2.0 litros está disponible con la caja de cambios de paleta.",
                        "/img/car1.png",
                        17479));
        listaRetorno.add(
                new Auto(id++,
                        "New Beetle",
                        "Volkswagen",
                        "El Volkswagen Beetle se produce desde 1998. Está disponible como cupé o convertible. El VW Beetle está propulsado por una amplia gama de motores, incluidos dos turbodiésel de 1.9 litros y gasolina turbo de 1.8 litros. También hay un RSI de 3.2 litros disponible para la gama topper. ",
                        "/img/car2.png",
                        67540));
        listaRetorno.add(
                new Auto(id++,
                        "Golf V",
                        "Volkswagen",
                        "El Volkswagen Golf V se produce desde 2003. Existe una amplia gama de motores finos y fiables. La mejor opción sería gasolina de inyección directa FSI de 1.6 litros con 115 hp o turbodiesel de 2.0 litros con 150 hp. El último mencionado presenta un ahorro de combustible sobresaliente por su capacidad y velocidad de aceleración, aunque es un poco ruidoso. El de mayor rendimiento es un GTI de 2.0 litros, que entrega 200 hp, que continúa las tradiciones de los modelos de golf.",
                        "/img/car3.png",
                        78200));
        listaRetorno.add(
                new Auto(id++,
                        "Neon",
                        "Chrysler",
                        "Este automóvil se vende como Dodge Neon en los Estados Unidos y como Chrysler Neon para exportación únicamente. Es un Neon de segunda generación producido desde 2000.",
                        "/img/car4.png",
                        85400));
        
        return listaRetorno;
    }
    
    
    
}
