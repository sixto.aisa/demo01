package com.tecsup.back.service;

import com.tecsup.back.dao.AutoDAO;
import com.tecsup.back.domain.Auto;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author saisa
 */
public class AutoServiceImpl implements AutoService{
    
    

    @Override
    public List<Auto> lista() {
       return AutoDAO.data();
    }

    @Override
    public List<Auto> buscar(String criterio) {
        
        List<Auto> listaRetorno=new ArrayList<>();
        
        if(criterio==null || criterio.isEmpty()){
            return listaRetorno;
        }
        
        for(Auto a: AutoDAO.data()){
            if(a.getModelo().toUpperCase().contains(criterio.toUpperCase()) || a.getMarca().toUpperCase().contains(criterio.toUpperCase()) ){            
                listaRetorno.add(a);
            }            
        }
        

        return listaRetorno;
    }
    
}
