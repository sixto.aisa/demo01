package com.tecsup.back.service;

import com.tecsup.back.domain.Auto;
import java.util.List;

/**
 *
 * @author saisa
 */
public interface AutoService {
    
    public List<Auto> lista();    
    public List<Auto> buscar(String criterio);
    
    
}
